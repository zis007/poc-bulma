import Vue from "vue";
import App from "./App.vue";

require("@/assets/main.scss");
// if (window.isLightMode) {
//   require("@/assets/main.scss");
// } else {
//   require("@/assets/main-light.scss");
// }

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");

const buttonLight = document.getElementById("lightMode");

buttonLight.addEventListener("click", () => {
  console.log("light");
  require("@/assets/main-light.scss");
});
